db.createUser(
        {
            user: "mongouser",
            pwd: "pass",
            roles: [
                {
                    role: "readWrite",
                    db: "activitylog"
                }
            ]
        }
);